﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace resetuj_haslo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Resetuj hasło konta Active Directory";
            Console.WriteLine("\n\tNarzędzie do resetowania haseł kont domenowych w usłudze AD\n\t\tbitbucket.org/akastrau\n\t" +
                "==================================================================================");
            if (args.Length < 2)
            {
                Console.WriteLine("\n\tUżycie: " + AppDomain.CurrentDomain.FriendlyName + " nazwa_pliku_z_kontami.csv hasło");
                Console.Write("\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
                Console.ReadKey();
                Environment.Exit(1);
            }

            try
            {
                //Czytaj plik
                Console.WriteLine("\n\t[*] Otwieram plik...");
                var reader = new StreamReader(args[0], Encoding.Default);
                //Przygotuj liste - login
                List<string> samAccountName = new List<string>();
                //Haslo
                var password = args[1];

                Console.WriteLine("\t[*] Tworzę liste użytkowników...");

                while (!reader.EndOfStream)
                {
                    //Wczytaj pierwsza komorke jako nazwe uzytkownika
                    var name = reader.ReadLine().Split(',')[0];
                    //Podziel nazwe uzytkownika na imie i nazwisko
                    var samAccount = name.Split(' ');
                    //Jezeli nazwa uzytkownika zawiera imie - wyswietl imie
                    if (samAccount.Length < 2)
                    {
                        //Zamiana polskich znakow na zwykle
                        byte[] tempBytes;
                        tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(samAccount[0]);
                        string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);
                        var properName = asciiStr;
                        samAccountName.Add(properName);
                        //Console.WriteLine("\n\tUżytkownik: " + name + " Nazwa konta: " + properName + "\n");
                    }
                    else
                    {
                        byte[] tempBytes;
                        tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(samAccount[1]);
                        string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);
                        var properName = asciiStr + '-';
                        tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(samAccount[0]);
                        asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);
                        properName += asciiStr[0];
                        samAccountName.Add(properName);

                        //Console.WriteLine("\n\tUżytkownik: " + name + " Nazwa konta: " + properName + "\n");
                    }
                }

                Console.WriteLine("\n\t\t\tNazwa użytkownika\t\tLogin\n\t============================================================================\n");
                for (int i = 0; i != samAccountName.Count; i++)
                {
                    Console.WriteLine(String.Format("\t{0, 30}{1,30}", samAccountName[i], samAccountName[i]));
                }

                var answer = new ConsoleKeyInfo();
                Console.Write("\n\tCzy chcesz zresetować hasła dla tych kont? [T/N] ");
                answer = Console.ReadKey(false);

                while (answer.Key != ConsoleKey.T && answer.Key != ConsoleKey.N)
                {
                    Console.Write("\n\tCzy dodać te konta? [T/N]: ");
                    answer = Console.ReadKey(false);
                }

                if (answer.Key == ConsoleKey.T)
                {
                    long success = 0;

                    using (var pc = new PrincipalContext(ContextType.Domain))
                    {
                        Console.WriteLine();
                        for (int i = 0; i < samAccountName.Count; i++)
                        {
                            Console.Write("\t[*] Resetowanie hasła " + (i + 1) + " z " + samAccountName.Count + "\r");
                            UserPrincipal userPrincipalCheck = UserPrincipal.FindByIdentity(pc, samAccountName[i]);
                            if (userPrincipalCheck != null)
                            {
                                userPrincipalCheck.SetPassword(password);
                                userPrincipalCheck.ExpirePasswordNow();
                                userPrincipalCheck.Save();
                                userPrincipalCheck.Dispose();
                                success++;
                            }
                           
                        }
                    }
                    Console.WriteLine("\n\n\tZresetowano haseł " + (success));
                    if (success - samAccountName.Count != 0)
                    {
                        Console.WriteLine("\n\n\tNie udało się zresetować hasła dla niektórych użytkowników\n");
                    }
                }


            }
            catch (Exception ex)
            {
                Console.Write("\n\t" + ex.Message + "\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
                Console.ReadKey();
                Environment.Exit(1);
            }
            Console.Write("\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
            Console.ReadKey();
        }
    }
}
